var latitude = 48.290877;
var longitude = 14.287759;
var map;
var marker;
var marker_end;
var route;
var radius = 2600;
var apikey = "5b3ce3597851110001cf6248ae592bc5d91c46b3955c59e8ec988e0b";
var startIcon = L.icon({
    iconUrl: 'assets/marker_start.png',

    iconSize: [150, 95], // size of the icon
    popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
});
var feuerwehrIcon = L.icon({
    iconUrl: 'assets/marker_feuerwehr.png',

    iconSize: [150, 95], // size of the icon
    popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
});
var policeIcon = L.icon({
    iconUrl: 'assets/marker_polizei.png',

    iconSize: [150, 95], // size of the icon
    popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
});
var hospitalIcon = L.icon({
    iconUrl: 'assets/marker_krankenhaus.png',

    iconSize: [150, 95], // size of the icon
    popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
});
window.addEventListener("load", function () {
    map = L.map("map").setView([latitude, longitude], 13);
    L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", { maxZoom: 19 }).addTo(map);
});

function getLocation(type) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
        getData(type);

    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
    if (marker != undefined) { map.removeLayer(marker); }
    marker = L.marker([latitude, longitude], { icon: startIcon }).addTo(map);
}

function getData(typ) {
    var opl = new L.OverPassLayer({
        minZoom: 10,
        "query": `nwr["amenity"="${typ}"](around:${radius},${latitude},${longitude}); out tags center qt;`,
        onSuccess: (data) => {
            var promises = data.elements.map((element) => (
                getRoute(element, [longitude, latitude], [element.lon ?? element.center.lon, element.lat ?? element.center.lat])
            ));
            Promise.all(promises).then((routingresults) => {
                console.log(routingresults);
                routingresults.sort((a, b) => (a.route.features[0].properties.summary.duration - b.route.features[0].properties.summary.duration));
                if (marker_end != undefined) { map.removeLayer(marker_end); }
                if (route != undefined) { map.removeLayer(route); }
                var endIcon;
                if (typ == "police") { endIcon = policeIcon }
                if (typ == "fire_station") { endIcon = feuerwehrIcon }
                if (typ == "hospital") { endIcon = hospitalIcon }
                const result = routingresults[0];
                marker_end = L.marker([result.data.lat ?? result.data.center.lat, result.data.lon ?? result.data.center.lon], { icon: endIcon }).addTo(map);
                route = L.geoJSON(result.route).addTo(map);
                this.document.getElementById("contacts").innerHTML =
                    (result.data.tags['name']) + "<br>" +
                    (result.data.tags['contact:phone']) + "<br>" +
                    "<a href='" + (result.data.tags['contact:website']) + "'>"
                    + (result.data.tags['contact:website']) + "</a>"
            });

        }
    });
    map.addLayer(opl);
}

async function getRoute(data, startPosition, endPosition) {
    const response = await fetch(`https://api.openrouteservice.org/v2/directions/driving-car?api_key=${apikey}&start=${startPosition.join(",")}&end=${endPosition.join(",")}`, { headers: { "Content-type": "application/json" } });
    const json = await response.json();
    return {
        data: data,
        route: json
    };
}